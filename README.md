# benyanke Homebrew Tap

To add this repo to your tap list:

```shell
brew tap benyanke/tools https://gitlab.com/benyanke/homebrew-tools.git
```

Then install a tool from the list below by running:

```shell
brew install benyanke/tools/[toolname]
```

## Tools 

 - [ical-stats](https://gitlab.com/benyanke/ical-stats) - provides simplified timecard tracking
 - [kionctl](https://gitlab.com/benyanke/kionctl) - Kion/CloudTamer API client
 - [confpub](https://gitlab.com/benyanke/confpub) - Confluence CLI
