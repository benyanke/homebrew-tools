class Confpub < Formula
  # These files are recreated on every release, to make changes, edit templates/kionctl.rb.j2
  desc "Confluence CLI"
  homepage "https://gitlab.com/benyanke/confpub/"
  version "v2.3.1"

  if OS.mac? && Hardware::CPU.intel?
    sha256 "9c509fde54dc93d56e397367d4443be9a8b5a8e0feb372ff21c61f8fb44d0119"
    url "https://gitlab.com/api/v4/projects/60263714/packages/generic/releases/" + version + "/confpub_darwin_amd64"
  end

  if OS.mac? && Hardware::CPU.arm?
    sha256 "58ba53e05d7f87a49a3bc1301867340c12352166478862863fb0af384b1bb5c1"
    url "https://gitlab.com/api/v4/projects/60263714/packages/generic/releases/" + version + "/confpub_darwin_arm64"
  end

  if OS.linux? && Hardware::CPU.intel?
    sha256 "214e207800fbb0d74acef888e3cec0524c09e4cedccd8dbb007fca2cbd10ff22"
    url "https://gitlab.com/api/v4/projects/60263714/packages/generic/releases/" + version + "/confpub_linux_amd64"
  end

  if OS.linux? && Hardware::CPU.arm?
    sha256 "b8e31da44a35739c2eb88eacf333c415c713669a125070c200d7bcaf4fbb633e"
    url "https://gitlab.com/api/v4/projects/60263714/packages/generic/releases/" + version + "/confpub_linux_arm64"
  end

  def install
    file = Dir.glob("confpub_*").first
    bin.install file => "confpub"
  end 

  # TODO - improve test somehow
  test do
    confpub || true
  end

end
