class IcalStats < Formula
  # These files are recreated on every release, to make changes, edit templates/ical-stats.rb.j2
  desc "ical statistics tool for timecard tracking"
  homepage "https://gitlab.com/benyanke/ical-stats/"
  version "v1.8.0"

  if OS.mac? && Hardware::CPU.intel?
    sha256 "9c75676cac0e4d21c9bb2ab7885e6e7bf9bc37577ebf0cbe41dd03410795fdef"
    url "https://gitlab.com/api/v4/projects/28308303/packages/generic/releases/" + version + "/ical-stats_darwin_amd64"
  end

  if OS.mac? && Hardware::CPU.arm?
    sha256 "933c409c056cafe132dbf136bcc2c69b9994d3040603518357ae15bd6b04a356"
    url "https://gitlab.com/api/v4/projects/28308303/packages/generic/releases/" + version + "/ical-stats_darwin_arm64"
  end

  if OS.linux? && Hardware::CPU.intel?
    sha256 "d3c69454fab161a36d0a04a24136aefc840fb08e3487784ac48c01ec9b503baa"
    url "https://gitlab.com/api/v4/projects/28308303/packages/generic/releases/" + version + "/ical-stats_linux_amd64"
  end

  if OS.linux? && Hardware::CPU.arm?
    sha256 "364cce466a14db5a12fec99e161225648cd0d6b998506038f5489527b4129fa0"
    url "https://gitlab.com/api/v4/projects/28308303/packages/generic/releases/" + version + "/ical-stats_linux_arm64"
  end

  def install
    file = Dir.glob("ical-stats_*").first
    bin.install file => "ical-stats"
  end 

  # TODO - improve test somehow
  test do
    ical-stats || true
  end

end
