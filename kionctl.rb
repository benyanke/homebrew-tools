class Kionctl < Formula
  # These files are recreated on every release, to make changes, edit templates/kionctl.rb.j2
  desc "Kion/CloudTamer CLI"
  homepage "https://gitlab.com/benyanke/kionctl/"
  version "v3.9.11"

  if OS.mac? && Hardware::CPU.intel?
    sha256 "4b9a70bb96c3d4c5e2960aad00c31b10e12f7c8c0fb19ed10b847342e169b655"
    url "https://gitlab.com/api/v4/projects/37687737/packages/generic/releases/" + version + "/kionctl_darwin_amd64"
  end

  if OS.mac? && Hardware::CPU.arm?
    sha256 "34790d34411bf6c77c45ab3f4b5666d2c3cb45ea5d228aa9ef2d0da8ced43055"
    url "https://gitlab.com/api/v4/projects/37687737/packages/generic/releases/" + version + "/kionctl_darwin_arm64"
  end

  if OS.linux? && Hardware::CPU.intel?
    sha256 "26620297647ed10f54b8a650dfba35cd81b567de33a10fadf0d9f8c5334e9ee0"
    url "https://gitlab.com/api/v4/projects/37687737/packages/generic/releases/" + version + "/kionctl_linux_amd64"
  end

  if OS.linux? && Hardware::CPU.arm?
    sha256 "42c47aac7703b749de570e668e594bcfdbd42f897d2ca33096f07ed3d6193831"
    url "https://gitlab.com/api/v4/projects/37687737/packages/generic/releases/" + version + "/kionctl_linux_arm64"
  end

  def install
    file = Dir.glob("kionctl_*").first
    bin.install file => "kionctl"
  end 

  # TODO - improve test somehow
  test do
    kionctl || true
  end

end
